<?php
#Name:Multiply Large Number v1
#Description:Multiply a large number (such as those exceeding PHP max interger) by another large number. Returns the number as a string on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'number1' (required) is a string containing the number to be multiplied. 'number2' (required) is a string containing the number to multiply by. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):number1:string:required,number2:string:required,display_errors:bool:optional
#Content:
if (function_exists('multiply_large_number_v1') === FALSE){
function multiply_large_number_v1($number1, $number2, $display_errors = NULL){
	$errors = array();
	$progress = '';
	##Arguments
	if ($number1 == '0'){
		#Do nothing to avoid ctype_digit thinking 0 === FALSE
	} else if (@ctype_digit($number1) === FALSE){
		$errors[] = "number1";
	}
	if ($number2 == '0'){
		#Do nothing to avoid ctype_digit thinking 0 === FALSE
	} else if (@ctype_digit($number2) === FALSE){
		$errors[] = "number2";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	}
	if ($display_errors === TRUE OR $display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Decrement by $number1 by 1, and for each decrement increment $result by $number2, until $number1 reaches 0.]
	if (@empty($errors) === TRUE){
		$location = realpath(dirname(__FILE__));
		$result = '0';
		do {
			require_once $location . '/dependencies/decrement_large_number_v1.php';
			$number1 = @decrement_large_number_v1($number1, FALSE);
			$i = $number2;
			do {
				require_once $location . '/dependencies/decrement_large_number_v1.php';
				$i = @decrement_large_number_v1($i, FALSE);
				require_once $location . '/dependencies/increment_large_number_v1.php';
				$result = @increment_large_number_v1($result, FALSE);
			} while ($i != '0');
		} while ($number1 != '0');
	}
	result:
	##Display Errors
	if ($display_errors === TRUE and @empty($errors === FALSE)){
		$message = @implode(", ", $errors);
		if (function_exists('multiply_large_number_v1_format_error') === FALSE){
			function multiply_large_number_v1_format_error($errno, $errstr){
				echo $errstr;
			}
		}
		set_error_handler("multiply_large_number_v1_format_error");
		trigger_error($message, E_USER_ERROR);
	}
	##Return
	if (@empty($errors) === TRUE){
		return $result;
	} else {
		return FALSE;
	}
}
}
?>